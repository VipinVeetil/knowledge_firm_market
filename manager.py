from __future__ import division
import random
import parameters as p
import copy

class manager(object):
    def __init__(self):
        self.rank = 0
        self.subordinates = []
        self.knowledge = []

    # managers report the knowledge they have about their subordiantes
    def report(self):
        inputs = []
        for value in self.knowledge:
            if random.uniform(0,1) < p.tacitness:
                inputs.append(False)
            else:
                inputs.append(copy.copy(value))
        return inputs

    # ask the manager to collect knowledge from subordinates
    def collect_knowledge(self, which):
        excess = self.subordinates[which].report()
        if self.rank == 1:
            self.knowledge.append(excess)
        else:
            for value in excess:
                self.knowledge.append(value)



