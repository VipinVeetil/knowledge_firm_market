from __future__ import division
import random
import matplotlib.pyplot as plt

import parameters as p
import worker as w
import market as m
import firm as f


class main(object): # the class main. This creates the firms and the market and runs them forward in time
    def __init__(self):
        self.market = 0 # the market
        self.tall_firm = 0 # the tall firm
        self.short_firm = 0 # the short firm
        self.market_output = [] # a list of market's output over different simulation runs
        self.tall_firm_output = [] # a list of tall firms's output over different simulation runs
        self.short_firm_output = [] # a list of short firm's output over different simulation runs
    
    def run_market(self): # create and run market forward in time and produce output
        self.market = m.market() # create market
        self.market.process() # run all  market processes
        self.market_output.append(self.market.output) # append market's output to list of market's output
    
    def run_tall_firm(self): # create and run tall firm forward in time and produce
        self.tall_firm = f.firm() # create a firm
        self.tall_firm.r = p.r_tall # assign the firm r for the tall firm
        self.tall_firm.k = p.k_tall # assign the firm k for the tall firm
        self.tall_firm.process() # run all the firm processes
        self.tall_firm_output.append(self.tall_firm.output) # append tall firm's to list of tall firm's output
    
    def run_short_firm(self): # create and run short firm forward in time and produce
        self.short_firm = f.firm() # create a firm
        self.short_firm.r = p.r_short # assign the firm r for the short firm
        self.short_firm.k = p.k_short # assign the firm k for the short firm
        self.short_firm.process() # run all the firm processes
        self.short_firm_output.append(self.short_firm.output) # append short firm's to list of short firm's output

    def iterations(self, which): # runs the market, tall, or short firm depending on parameter value "which"
        if which == "market":
            for i in xrange(p.iterations):
                self.run_market()
        elif which == "tall_firm":
            for i in xrange(p.iterations):
                self.run_tall_firm()
        elif which == "short_firm":
            for i in xrange(p.iterations):
                self.run_short_firm()
        elif which == "all":
            for i in xrange(p.iterations):
                self.run_market()
                self.run_tall_firm()
                self.run_short_firm()

