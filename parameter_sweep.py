from __future__ import division
import parameters as p
import main
import numpy as np
import csv
import matplotlib.pyplot as plt

def parameter_sweep():
    
    increment_dynamism = p.simulation_increments
    range_dynamism=np.arange(0.0,1.01,increment_dynamism)
    increment_tacitness = p.simulation_increments
    range_tacitness=np.arange(0.0,1.01,increment_tacitness)

    with open('market_dynamism.csv','wb') as csvfile:
        for dynamism in range_dynamism:
            print('market dynamism', dynamism)
            p.dynamism = dynamism
            p.tacitness = 0
            main_instance = main.main()
            main_instance.iterations("market")
            writer=csv.writer(csvfile,delimiter=',')
            writer.writerow([dynamism]+main_instance.market_output)
        print('done market dynamism')


    with open('market_tacitness.csv','wb') as csvfile:
        for tacitness in range_tacitness:
            print('market tacitness', tacitness)
            p.dynamism = 0
            p.tacitness = tacitness
            main_instance = main.main()
            main_instance.iterations("market")
            writer=csv.writer(csvfile,delimiter=',')
            writer.writerow([tacitness]+main_instance.market_output)
        print('done market tacitness')
    
    with open('tall_firm_dynamism.csv','wb') as csvfile:
        for dynamism in range_dynamism:
            print('tall_firm dynamism', dynamism)
            p.dynamism = dynamism
            p.tacitness = 0
            main_instance = main.main()
            main_instance.iterations("tall_firm")
            writer=csv.writer(csvfile,delimiter=',')
            writer.writerow([dynamism] + main_instance.tall_firm_output)
        print('done tall_firm dynamism')
   
    with open('short_firm_dynamism.csv','wb') as csvfile:
        for dynamism in range_dynamism:
            print('short_firm dynamism', dynamism)
            p.dynamism = dynamism
            p.tacitness = 0
            main_instance = main.main()
            main_instance.iterations("short_firm")
            writer=csv.writer(csvfile,delimiter=',')
            writer.writerow([dynamism]+main_instance.short_firm_output)
        print('done short_firm dynamism')
   
    with open('tall_firm_tacitness.csv','wb') as csvfile:
        for tacitness in range_tacitness:
            print('tall_firm tacitness', tacitness)
            p.dynamism = 0
            p.tacitness = tacitness
            main_instance = main.main()
            main_instance.iterations("tall_firm")
            writer=csv.writer(csvfile,delimiter=',')
            writer.writerow([tacitness]+main_instance.tall_firm_output)
        print('done tall_firm tacitness')

    with open('short_firm_tacitness.csv','wb') as csvfile:
        for tacitness in range_tacitness:
            print('short_firm tacitness', tacitness)
            p.dynamism = 0
            p.tacitness = tacitness
            main_instance = main.main()
            main_instance.iterations("short_firm")
            writer=csv.writer(csvfile,delimiter=',')
            writer.writerow([tacitness]+main_instance.short_firm_output)
        print('done short_firm tacitness')

    increment_dynamism = 0.001
    range_dynamism=np.arange(0.198,0.201,increment_dynamism)
    increment_tacitness=0.001
    range_tacitness=np.arange(0.0,0.201,increment_tacitness)
    iterations = 10
  
    with open('m1.csv','wb') as csvfile:
        for dynamism in range_dynamism:
            for tacitness in range_tacitness:
                print('market', dynamism, tacitness)
                p.iterations = iterations
                p.dynamism = dynamism
                p.tacitness = tacitness
                main_instance = main.main()
                main_instance.iterations("market")
                writer=csv.writer(csvfile,delimiter=',')
                writer.writerow([dynamism]+[tacitness]+main_instance.market_output)
        print('done market')
    
    with open('tall_firm.csv','wb') as csvfile:
        for dynamism in range_dynamism:
            for tacitness in range_tacitness:
                print('tall_firm', dynamism, tacitness)
                p.iterations = iterations
                p.dynamism = dynamism
                p.tacitness = tacitness
                main_instance = main.main()
                main_instance.iterations("tall_firm")
                writer=csv.writer(csvfile,delimiter=',')
                writer.writerow([dynamism]+[tacitness]+main_instance.tall_firm_output)
        print('done tall_firm')
    
    with open('short_firm.csv','wb') as csvfile:
        for dynamism in range_dynamism:
            for tacitness in range_tacitness:
                print('short_firm', dynamism, tacitness)
                p.iterations = iterations
                p.dynamism = dynamism
                p.tacitness = tacitness
                main_instance = main.main()
                main_instance.iterations("short_firm")
                writer=csv.writer(csvfile,delimiter=',')
                writer.writerow([dynamism]+[tacitness]+main_instance.short_firm_output)
        print('done short_firm')

    with open('market_trade.csv','wb') as csvfile:
        p.dynamism = 0
        p.tacitness = 0
        p.iterations = 1
        p.record_market_optimality = True
        p.time_steps_market = 1000
        main_instance = main.main()
        main_instance.iterations("market")
        gains_from_trade = []
        initial = main_instance.market.workers_optimality[0]
        for i in main_instance.market.workers_optimality:
            gains_from_trade.append(1 - (i / initial))
        writer=csv.writer(csvfile,delimiter=',')
        writer.writerow(gains_from_trade)
        print('market optimality')
    

parameter_sweep()
