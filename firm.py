from __future__ import division
import random
import copy
import numpy as np

import parameters as p
import worker as w
import manager as m


class firm(object):
    # the class of firms
    def __init__(self):
        self.workers = []
        # firm has a list of workers
        self.managers = []
        # firm has a list of managers
        self.firm = []
        # firm has a list of all agents in the firm including all workers, managers, and the entrepreneur-coordinator
        self.k = 0
        # k denote the number of subordinates of each manager
        self.r = 0
        # r denotes the rank of the entrepreneur-coordinator, and therefore the number of layers of hierarchy within the firm
        self.entrepreneur = None
        # firm has an entrepreneur-coordinator
        self.output = 0
        # the output of the firm
        
    def create_firm(self):
        self.entrepreneur = m.manager()
        # the entrepreneur-coordinator is a type of a manager
        self.entrepreneur.rank = self.r
        # the entrepreneur-coordinator has the highest rank
        self.firm.append(self.entrepreneur)
        # the following algorithm creates the firm with assigning subordinates
        for rank in xrange(self.r):
            agents = []
            for manager in self.firm:
                if len(manager.subordinates) == 0:
                    if manager.rank > 1:
                        manager.subordinates = [m.manager() for i in range(self.k)]
                        for k in manager.subordinates:
                            k.rank = manager.rank - 1
                            agents.append(k)
                    # if the manager has rank 1, then the subordinates are workers
                    elif manager.rank == 1:
                        manager.subordinates = [w.worker() for i in range(self.k)]
                        for s in manager.subordinates:
                            s.rank = manager.rank - 1
                            agents.append(s)
            for agent in agents:
                self.firm.append(agent)

    def populate_workers(self):
        # append the workers in the first to the workers list
        count = 0
        for agent in self.firm:
            if agent.rank == 0:
                self.workers.append(agent)

    def change_input_workers(self):
    # the input of some workers change
        number_of_workers_shocked = int(p.dynamism * p.number_of_workers)
        # the number of workers whose input will change.
        if number_of_workers_shocked % 2 != 0:
            # if the number of workers selected is not even, add 1
            number_of_workers_shocked += 1
        if number_of_workers_shocked > p.number_of_workers:
            number_of_workers_shocked = p.number_of_workers
        shocked_workers = random.sample(self.workers, number_of_workers_shocked)
        # select the workers that will be shocked
        for worker in shocked_workers:
           worker.change_input()

    def record_goods(self):
        # managers pass knowledge of goods workers possess up the firm hierarchy, finally to the entrepreneur-coordinator
        for rank in xrange(self.r):
            managers_temp = []
            for agent in self.firm:
                if agent.rank == (rank+1):
                    managers_temp.append(agent)
            temp = []
            for time in xrange(self.k):
                self.change_input_workers()
                for manager in managers_temp:
                    manager.collect_knowledge(time)

    def reallocate_goods(self):
        # the entrepreneur-coordinator reallocates input among workers based on her knowledge
        for i in xrange(p.number_of_workers):
            worker = self.workers[i]
            excess = self.entrepreneur.knowledge[i]
            if excess != False:
                worker.input -= excess

    def produce(self):
        # workers produce goods
        self.output = 0
        for worker in self.workers:
            worker.produce()
            self.output += worker.output

    def process(self):
        # ask the firm to create structure, create workers, record inputs, reallocate inputs, and produce
        self.create_firm()
        self.populate_workers()
        self.record_goods()
        self.reallocate_goods()
        self.produce()
