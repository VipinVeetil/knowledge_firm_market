from __future__ import division
import random
import parameters as p 
import worker as w
import copy

class market(object):
    def __init__(self):
        # a list of workers in the market
        self.workers = []
        # market's output
        self.output = 0
        self.workers_optimality = []

    def create_workers(self):
        self.workers = [w.worker() for n in range(p.number_of_workers)]
    
    def change_input_workers(self):
        # the input of some workers change
        number_of_workers_shocked = int(p.dynamism * p.number_of_workers)
        # if the number of workers selected is not even, add 1
        if number_of_workers_shocked % 2 != 0:
            number_of_workers_shocked += 1
        if number_of_workers_shocked > p.number_of_workers:
            number_of_workers_shocked = p.number_of_workers
        # select the workers that will be shocked
        shocked_workers = random.sample(self.workers, number_of_workers_shocked)
        for worker in shocked_workers:
            worker.change_input()
 
    # trades happen through bilateral exchange in the market
    def trade(self):
        # shuffle the list of workers to have random matches every time step
        random.shuffle(self.workers)
        # divide workers into two sets to create bilateral matches
        half = int(p.number_of_workers / 2)
        first_set_workers = self.workers[:half]
        second_set_workers = self.workers[half:]
        # create bilateral match between workers
        for n in xrange(half):
            w0 = first_set_workers[n]
            w1 = second_set_workers[n]
            i0 = w0.report()
            i1 = w1.report()
            if i0 != False and i1 != False:
                if i0 < 0 and i1 > 0:
                    trade_quantity = min(-i0, i1)
                    trade_quantity = 0
                    w0.input += trade_quantity
                    w1.input -= trade_quantity
                elif i0 > 0 and i1 < 0:
                    trade_quantity = min(i0, -i1)
                    w0.input -= trade_quantity
                    w1.input += trade_quantity

    # ask workers to produce
    def produce(self):
        self.output = 0
        for worker in self.workers:
            worker.produce()
            self.output += worker.output
        # deduct exchange cost from output
        self.output = (1 - p.exchange_cost) * self.output
    
    def record_worker_optimality(self):
        count = 0
        for worker in self.workers:
            count += abs(worker.input)
        self.workers_optimality.append(count)
    
    # the market-process creates workers, gets them to trade et al
    def process(self):
        self.create_workers()
        for time in xrange(p.time_steps_market):
            self.change_input_workers()
            self.trade()
            if p.record_market_optimality == True:
                self.record_worker_optimality()
        self.produce()
