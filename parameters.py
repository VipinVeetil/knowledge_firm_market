
r_tall = 4
r_short = 2
k_tall = 10
k_short = 100
time_steps_market = 100
dynamism = 0
tacitness = 0
number_of_workers = 10000
exchange_cost = 0.4
endowment_unif_dist = 1000
dynamism_unif_dist = 1
iterations = 100
record_market_optimality = False
simulation_increments = 0.01
