from __future__ import division
import pandas as pd
import matplotlib.pyplot as plt
import random as random
import numpy as np
import parameters as p

size = int(1 / p.simulation_increments) + 1

#Dynamism of knowledge plot
market_dynamism = pd.read_csv('market_dynamism.csv',header=None)
market_dynamism_values = market_dynamism.iloc[:,1:]
market_dynamism_values = market_dynamism_values.divide(10000)
tall_dynamism = pd.read_csv('tall_firm_dynamism.csv',header=None)
tall_dynamism_values = tall_dynamism.iloc[:,1:]
tall_dynamism_values = tall_dynamism_values.divide(10000)
short_dynamism = pd.read_csv('short_firm_dynamism.csv',header=None)
short_dynamism_values = short_dynamism.iloc[:,1:]
short_dynamism_values = short_dynamism_values.divide(10000)

dynamism = list(market_dynamism.iloc[:,0])

y_market = []
for i in xrange(size):
    y_market.append(list(market_dynamism_values.iloc[i,:]))

y_tall = []
for i in xrange(size):
    y_tall.append(list(tall_dynamism_values.iloc[i,:]))

y_short = []
for i in xrange(size):
    y_short.append(list(short_dynamism_values.iloc[i,:]))


for xe, ye in zip(dynamism, y_market):
    plt.scatter([xe] * len(ye), ye, color = 'black', marker = '.', s = 10, alpha = 1)
for xe, ye in zip(dynamism, y_tall):
    plt.scatter([xe] * len(ye), ye, color = 'tomato', marker = '.', s = 10, alpha = 1)


for xe, ye in zip(dynamism, y_short):
    plt.scatter([xe] * len(ye), ye, color = 'deepskyblue', marker = '.', s = 10, alpha = 1)


plt.xlabel('Dynamism of knowledge')
plt.ylabel('Output')
plt.ylim([0,1])
plt.xlim([0,1.1])
plt.text(0.7, 0.90, 'market in black', color = 'black')
plt.text(0.7, 0.85, 'tall firm in red', color = 'tomato')
plt.text(0.7, 0.80, 'short firm in blue', color = 'deepskyblue')
plt.title('Variation in output with dynamism of knowledge')
plt.savefig('dynamism.png', bbox_inches='tight')
plt.close()

#Tacitness of knowledge plot
market_tacitness = pd.read_csv('market_tacitness.csv',header=None)
market_tacitness_values = market_tacitness.iloc[:,1:]
market_tacitness_values = market_tacitness_values.divide(10000)
tacitness = list(market_tacitness.iloc[:,0])

tall_tacitness = pd.read_csv('tall_firm_tacitness.csv',header=None)
tall_tacitness_values = tall_tacitness.iloc[:,1:]
tall_tacitness_values = tall_tacitness_values.divide(10000)

short_tacitness = pd.read_csv('short_firm_tacitness.csv',header=None)
short_tacitness_values = short_tacitness.iloc[:,1:]
short_tacitness_values = short_tacitness_values.divide(10000)


y_market = []
for i in xrange(size):
    y_market.append(list(market_tacitness_values.iloc[i,:]))

y_tall = []
for i in xrange(size):
    y_tall.append(list(tall_tacitness_values.iloc[i,:]))

y_short = []
for i in xrange(size):
    y_short.append(list(short_tacitness_values.iloc[i,:]))


for xe, ye in zip(tacitness, y_market):
    plt.scatter([xe] * len(ye), ye, color = 'black', marker = '.', s = 10, alpha = 0.25)

for xe, ye in zip(tacitness, y_tall):
    plt.scatter([xe] * len(ye), ye, color = 'tomato', marker = '.', s = 10, alpha = 0.25)

for xe, ye in zip(tacitness, y_short):
    plt.scatter([xe] * len(ye), ye, color = 'deepskyblue', marker = '.', s = 10, alpha = 0.25)


plt.xlabel('Tacitness of knowledge')
plt.ylabel('Output')
plt.ylim([0,1])
plt.xlim([0,1.1])
plt.text(0.7, 0.90, 'market in black', color = 'black')
plt.text(0.7, 0.85, 'tall firm in red', color = 'tomato')
plt.text(0.7, 0.80, 'short firm in blue', color = 'deepskyblue')
plt.title('Variation in output with tacitness of knowledge')
plt.savefig('tacitness.png', bbox_inches='tight')
plt.close()


# Summary plot

market = pd.read_csv('market.csv',header=None)
tall = pd.read_csv('tall_firm.csv',header=None)
short = pd.read_csv('short_firm.csv',header=None)

market_values = market.iloc[:,2:]
tacitness = market.iloc[:,1]
mean_market = list(market_values.mean(axis = 1))
tall_values = tall.iloc[:,2:]
dynamism = tall.iloc[:,0]
mean_tall = list(tall_values.mean(axis = 1))
short_values = short.iloc[:,2:]
mean_short = list(short_values.mean(axis = 1))

coordinates = []
for i in xrange(len(dynamism)):
    coordinates.append((dynamism[i], tacitness[i]))



which_greater = []
for i in xrange(len(dynamism)):
    if mean_market[i] == max(mean_market[i], mean_tall[i], mean_short[i]):
        which_greater.append(0)
    elif mean_tall[i] == max(mean_market[i], mean_tall[i], mean_short[i]):
        which_greater.append(1)
    else:
        which_greater.append(2)


summary = []
for i in xrange(len(which_greater)):
    summary.append((coordinates[i], which_greater[i]))

market_highest = []
for i in summary:
    if i[1] == 0:
        market_highest.append(i[0][1])
    else:
        market_highest.append(-1)

tall_highest = []
for i in summary:
    if i[1] == 1:
        tall_highest.append(i[0][1])
    else:
        tall_highest.append(-1)

short_highest = []
for i in summary:
    if i[1] == 2:
        short_highest.append(i[0][1])
    else:
        short_highest.append(-1)


short_highest[0] = 0

plt.scatter(dynamism, market_highest, marker = '.', s = 10, color = 'black', alpha = 0.4)
plt.scatter(dynamism, tall_highest, marker = '.', s = 10, color = 'tomato', alpha = 0.4)
plt.scatter(dynamism, short_highest, marker = '.', s = 10, color = 'deepskyblue', alpha = 0.4)

plt.ylim(-0.001,0.20)
plt.xlim(-0.001,0.20)
plt.title('Performance of the institutions with variation in the nature of knowledge')
plt.xlabel('Dynamism of knowledge')
plt.ylabel('Tacitness of knowledge')
plt.legend(('market', 'tall firm', 'short firm'), fontsize = 11, markerscale = 0.8)
plt.savefig('summary.png', bbox_inches='tight')
plt.close()


# Market gains from trade plot
gains_from_trade = pd.read_csv('market_trade.csv',header=None)
gains_from_trade = list(gains_from_trade.iloc[0,:])
print gains_from_trade[99]
plt.scatter(xrange(len(gains_from_trade)), gains_from_trade, marker = '.', s = 10, color = 'black')
#plt.text(50, 0.70, 'The market exhaust 95% of gains from trade by 100th time step', color = 'black', fontsize = 10)
plt.ylim([-0.01,1.01])
plt.xlim([-1,201])
plt.title('Market gains from trade over time')
plt.xlabel('Time steps')
plt.ylabel('Proportion gains from trade exhausted')
plt.savefig('market_trade.png', bbox_inches='tight')
plt.close()








