from __future__ import division
import random
import copy
import math

import parameters as p

class worker(object):
    # the worker class
    def __init__(self):
        self.input = random.uniform(-p.endowment_unif_dist,p.endowment_unif_dist)
        # workers begin with a random quantity of input
        self.output = 0
    
    def report(self):
        # return the workers input, use parameter tacitness to decide when not to report
        if random.uniform(0,1) < p.tacitness:
            return False
        else:
            excess = copy.copy(self.input)
            return excess

    def change_input(self): # change worker's input when knowledge is dynamic
        self.input += random.uniform(-p.dynamism_unif_dist,p.dynamism_unif_dist)
    
    def produce(self): # produce output
        self.output = math.e ** (-abs(self.input))
