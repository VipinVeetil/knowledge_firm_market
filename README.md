# README #

These files pertain to the paper titled "Use of knowledge in the Market and the Firm". The paper is available on Vipin Veetil's Social Science Research Network Page: https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2600625
The code is run using parameter_sweep.py. This will produce several csv files with data. The data can be plotted into figures using analysis.py.


If you have any queries or suggestions please write to me at vipin.veetil@gmail.com
You do not need to credit me for using any or all of this code